# React Native Exercise

[![N|Solid](https://static.wikia.nocookie.net/tensei-shitara-slime-datta-ken/images/3/34/Rimuru_Slime_Anime.png/revision/latest/scale-to-width-down/340?cb=20180922214304)](https://github.com/adanamanya)
[![forthebadge made-with-javascript](https://forthebadge.com/images/badges/made-with-javascript.svg)](https://www.reactjs.org/)

This repo was built for test purpose, Here are the feature list.

     a. User auth & register
     b. Item management
     c. User can only update or delete their own item
     d. Dashboard to manage user & item
     e. Unit test ( jest & react native testing library)
     f. Twelve-factor app vibes (https://12factor.net) (using react-native-dotenv)
     g. commitizen
     h. pre-commit
     i. expo
     j. Persistent Login Credentials
  
- To access the dashboard, please login to gmail gilangdevmail777@gmail.com, pass: bukanrahasia123

  - Firestore (db)
  https://console.firebase.google.com/u/1/project/wehehehe-c0bcf/firestore
  - auth management
  https://console.firebase.google.com/u/1/project/wehehehe-c0bcf/authentication/users

#### How to run

##### Prod
- install expo apps on android/ios devices
    android: https://play.google.com/store/apps/details?id=host.exp.exponent
    ios: https://apps.apple.com/us/app/expo-client/id982107779
- open up this url https://expo.io/@ada.namanya/wehehehe
- open project on expo apps, voila!!

Notes: if u can't open up from ios, probably there is an issue on expo permission, some ios version might need to login to open projects(user can only open up their own project). In case this happen, please login using gilangdevmail777@gmail.com, pass: bukanrahasia123
##### Dev
This repo requires [Node.js](https://nodejs.org/) ,[Expo](https://expo.io) to run.
- please also make sure android simulator and ios simulator was installed and ready.
- Expo.io is just a tool to run it, u need to install android simulator(installable from android studio), and ios simulator(from xcode) on laptop/pc.

Install the dependencies and devDependencies and start the expo.

```sh
$ npm install --global expo-cli
$ cd (foldername)
$ npm i
$ expo start
$ i (run on ios simulator)
$ a (run on android simulator)
```

