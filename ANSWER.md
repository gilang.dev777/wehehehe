### ANSWERS

* [1] - Done on this repo
* [2] - A monolithic architecture is built as into one large system and is usually one code-base. A monolith is often deployed all at once, regardless of what was changed. 
A microservices architecture however is where an app is built as a suite of small services, each with their own code-base. These services are built around specific capabilities and are usually independently deployable. In this case, we can assume this repo was using monolithics, since the approach of microservice usually applied on backend, for frontend it was called microfrontend, this microfrontend was using the same approach as microservices but with more adaptation for frontend.
![N|Solid](https://miro.medium.com/max/4188/1*Cs_FIEMCVn6DrO8-rZjtcw.png)

* [3] - Guarding by pre commit and commitizen, these are following plugin i used to guard it.

| Plugin | Description |
| ------ | ------ |
| Jest & react-native-testing library | Unit Testing|
| Prettier & Eslint | Code Formatter & Linter|
| react native dotenv | Bringing up 12factor|
|pre-commit(husky) | To run some script before commit, can be used for adding test before commit, so if there is any new error caused by the new commited code, should be covered by unit test.|
| commitizen| Adding more netizen styled for commit, to make commits more friendly to be manageable, can be used with ticket number from JIRA into descriptions, prevent lost track of each commit when there is a lot of devs in team, and pretty fast development. For usual format, should be adding Ticket number from JIRA, fill the description of commit like is it bug fix, feature, or etc, and is it possibly affecting another features or not, thus can also help to prevent bugs from someone replacing another's code, or someone new features affecting another's code and causing error, without anyone noticing. In short, this can help everyone keep in track for any updates, thus can prevent unknown bug because of misscom, because description can be much helpful for PR check. |
* [4] - Im sorry, im not really understand of the question, the task is to create frontend apps, no backend service there, and no need to create services. If u refering into communicating between native modules and js modules, it can be using bridging approach, i will create native modules and extend it from the react native bridging modules, hence this will communicating native modules with the js,

* [5] synchronous code is executed in sequence – each statement waits for the previous statement to finish before executing. Asynchronous code doesn’t have to wait.
Javascript is synchronous, thus anytime there is a function needs to run in async, should declare async await on it. For, example : using it for fetch data function on react, async fetch, then await for response, basicly it is telling to stop executing until the promise is resolved.
* [6] im using expo.io, and developing this on macbook, hence i can run both android and ios emulator to test, but to test it is working on productions, im using expo, i publish it to expo, then open the url from real ios&android devices, expo just make it easier for cross-platform react native.
* [7] Moving env to .env(based on 12factor), using auth method from firebase, which already secured in https and also implement rules on firestore,
here are the rules:
```javascript
rules_version = '2';
service cloud.firestore {
  match /databases/{database}/documents {
   match /{users=**} {
        allow read, write: if Loggedin();
    }
  
  match /entities/{id} {
        allow read: if Loggedin() && resourceMatchesAuthorID();
        allow write: if Loggedin();
        allow update, delete: if Loggedin() && resourceMatchesAuthorID();
    }

    function Loggedin() {
        return request.auth != null;
    }

    function resourceMatchesAuthorID() {
        return request.auth.uid == resource.data.authorID;
    }
  }
  }
```
it means, only loggedIn user can update, read, write or delete, and they only can edit their own data, uid should be matched.
* [8] Just need to install the sdk, sdk should be supported nodejs, so can be running on both platform, ios and android, no need further tweak, saving development time. The approach will be as same as implementing firebase sdk into this apps, u can check on the firebase implementation code on this repo.



