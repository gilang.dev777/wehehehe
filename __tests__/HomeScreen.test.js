import 'react-native'
import React from 'react'
import { HomeScreen } from 'screen'
import { firebase } from 'firebaseconf/config'
import { fireEvent, render, act } from 'react-native-testing-library'
import { expect, it } from '@jest/globals'
describe('Homescreen unit test', () => {
  afterEach(() => {
    jest.restoreAllMocks()
  })
  it('Add post', async () => {
    const txt = 'hehee'
    const data = {
      text: txt,
      authorID: null,
      createdAt: null,
    }
    const { getByPlaceholder, getByText } = render(<HomeScreen />)
    const firestoreMock = {
      collection: jest.fn().mockReturnThis(),
      add: jest.fn().mockResolvedValueOnce(),
    }
    jest
      .spyOn(firebase, 'firestore')
      .mockImplementationOnce(() => firestoreMock)
    const postfield = getByPlaceholder(/Add new post/i)
    const button = getByText(/Add/i)
    await fireEvent.changeText(postfield, txt)
    await fireEvent.press(button)
    await act(async () => {
      expect(postfield.props.value).toEqual(txt)
    })
    expect(firestoreMock.collection).toBeCalledWith('entities')
    expect(firestoreMock.add).toBeCalledWith(data)
  })
})
