import React, { useEffect, useState, useLayoutEffect } from 'react'
import {
  FlatList,
  Keyboard,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Alert,
} from 'react-native'
import styles from './styles'
import { firebase } from 'firebaseconf/config'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import PropTypes from 'prop-types'

export default function HomeScreen(props) {
  const [entities, setEntities] = useState([])
  const [updated, setUpdated] = useState(false)
  const [entityText, setEntityText] = useState('')
  const [disabled, setDisabled] = useState(false)
  const entityRef = firebase.firestore().collection('entities')
  const { route, navigation, user } = props
  const userID = route?.params?.user.id ?? user?.id

  useLayoutEffect(() => {
    navigation?.setOptions({
      _headerRight: () => (
        <TouchableOpacity onPress={onLogout}>
          <Text style={styles.LOGOUT}>LOGOUT</Text>
        </TouchableOpacity>
      ),
      get headerRight() {
        return this._headerRight
      },
      set headerRight(value) {
        this._headerRight = value
      },
    })
  }, [navigation])
  useEffect(() => {
    if (userID) {
      entityRef
        .where('authorID', '==', userID)
        .orderBy('createdAt', 'asc')
        .onSnapshot(
          (querySnapshot) => {
            const newEntities = []
            querySnapshot.forEach((doc) => {
              const entity = doc.data()
              entity.id = doc.id
              newEntities.push(entity)
            })
            setEntities(newEntities)
          },
          (error) => {
            console.log(error)
          },
        )
    }
  }, [])

  const onAddButtonPress = () => {
    if (entityText && entityText.length > 0) {
      const timestamp = firebase.firestore?.FieldValue?.serverTimestamp()
      const data = {
        text: entityText,
        authorID: userID ? userID : null,
        createdAt: timestamp ? timestamp : null,
      }
      entityRef
        .add(data)
        .then((_doc) => {
          setEntityText('')
          Keyboard.dismiss()
        })
        .catch((error) => {
          Alert.alert(error)
        })
    }
  }

  const onLogout = () => {
    firebase
      .auth()
      .signOut()
      .then(() => {
        Alert.alert('sign out success')
        navigation.replace('Login', { user: null })
      })
      .catch((error) => {
        Alert.alert(error)
      })
  }

  const updateText = (text) => {
    const index = parseInt(text.slice(0, 1)) - 1
    let newArr = [...entities]
    setUpdated(text.slice(2) !== entities[index].text ? true : false)
    newArr[index].text = text.slice(2)
    setEntities(newArr)
  }

  const saveText = (index, text) => {
    if (updated) {
      entityRef
        .doc(entities[index].id)
        .update('text', text)
        .then(() => {
          Alert.alert('update success')
          setUpdated(false)
        })
        .catch((error) => {
          Alert.alert(error)
        })
    }
  }

  const deletePost = (index) => {
    setDisabled(true)
    entityRef
      .doc(entities[index].id)
      .delete()
      .then(() => {
        setDisabled(false)
        Alert.alert('delete success')
      })
      .catch((error) => {
        setDisabled(false)
        Alert.alert(error)
      })
  }

  const renderEntity = ({ item, index }) => {
    return (
      <View style={styles.entityContainer}>
        <TextInput
          style={styles.input}
          value={index + 1 + '.' + item.text}
          onChangeText={(text) => (disabled ? null : updateText(text))}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
          onEndEditing={() => (disabled ? null : saveText(index, item.text))}
        />
        <TouchableOpacity
          style={disabled ? styles.DisabledButton : styles.entityButton}
          onPress={() => (disabled ? null : deletePost(index))}
        >
          <Text style={styles.buttonText}>Delete</Text>
        </TouchableOpacity>
      </View>
    )
  }

  return (
    <View style={styles.container}>
      <View style={styles.formContainer}>
        <TextInput
          style={styles.input}
          placeholder="Add new post"
          placeholderTextColor="#aaaaaa"
          onChangeText={(text) => setEntityText(text)}
          value={entityText}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
        <TouchableOpacity style={styles.button} onPress={onAddButtonPress}>
          <Text style={styles.buttonText}>Add</Text>
        </TouchableOpacity>
      </View>
      {entities && (
        <KeyboardAwareScrollView>
          <View style={styles.listContainer}>
            <FlatList
              data={entities}
              renderItem={renderEntity}
              keyExtractor={(item) => item.id}
              removeClippedSubviews
            />
          </View>
        </KeyboardAwareScrollView>
      )}
    </View>
  )
}
HomeScreen.propTypes = {
  route: PropTypes.object,
  user: PropTypes.object,
  navigation: PropTypes.object,
}
